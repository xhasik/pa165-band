import json
from locust import HttpUser, SequentialTaskSet, between, task
from requests.exceptions import JSONDecodeError

class UserService(SequentialTaskSet):

    # Replace token with valid ones
    token = "eyJraWQiOiJyc2ExIiwidHlwIjoiYXQrand0IiwiYWxnIjoiUlMyNTYifQ.eyJhdWQiOiI3ZTAyYTBhOS00NDZhLTQxMmQtYWQyYi05MGFkZDQ3YjBmZGQiLCJzdWIiOiI0Njg5ODRAbXVuaS5jeiIsImFjciI6Imh0dHBzOi8vcmVmZWRzLm9yZy9wcm9maWxlL3NmYSIsInNjb3BlIjoiZWR1cGVyc29uX3Njb3BlZF9hZmZpbGlhdGlvbiB0ZXN0X3JlYWQgdGVzdF93cml0ZSBvcGVuaWQgZW1haWwgcHJvZmlsZSIsImF1dGhfdGltZSI6MTY4NDc3NzEzMiwiaXNzIjoiaHR0cHM6Ly9vaWRjLm11bmkuY3ovb2lkYy8iLCJleHAiOjE2ODQ3ODUwNDEsImlhdCI6MTY4NDc4MTQ0MSwiY2xpZW50X2lkIjoiN2UwMmEwYTktNDQ2YS00MTJkLWFkMmItOTBhZGQ0N2IwZmRkIiwianRpIjoiODQ5MmNjZTgtYTNiYS00NmI5LTg4NjEtNjU5MDJlNmQ4Yzg2In0.pUgnvwj07jsyYysW_NQgOt2dlsvtVbccXRVRfJzBvTBkUeqjwvADMh0FIlP9knyiEPECLCcWyW-NpFYkU1Knx14Socaq_YiLgBUUk1sby9yrclZAh_UCeoeSDd7wWQChcWwfdL9gXfshyr994vPfvvhH3xhfWQTKyRJ1VZC6_-gzfgfx1skrkcCVjcsXtjRZ9JP3ph9DjEkDPiBpiHRu2cWv1vnnN6V5utIhSwITQpWKc1I-B6ngDVJdsjeO4VCAIeDqjdWVDlHNwZwqeh--avpNM3oulcXMr3qn2FBHIDRkSvcdo4fHhyIl-6pOfh4SGNMM3CAghdEboOawzw4omA"
    auth_header = {"Authorization": "Bearer " + token }

    wait_time = between(1, 3)
    def create_users(self):
        # Create two users
        user1_response = self.client.post("http://localhost:9091/api/v1/users",
                                          json={"name": "Band manager",
                                                "email": "jdoe@email.com",
                                                "password": "1234"},
                                          headers = self.auth_header)
        user2_response = self.client.post("http://localhost:9091/api/v1/users",
                                          json={"name": "Team member",
                                                "email": "jdoe@gmail.com",
                                                "password": "1234"},
                                          headers = self.auth_header)

        user1_id = user1_response.json()["id"]
        user2_id = user2_response.json()["id"]

        self.user1_id = user1_id
        self.user2_id = user2_id

    def assign_band_manager(self):
        manager_response = self.client.post("http://localhost:9094/api/v1/band-manager",
                                            json={"nickname": "boss",
                                                  "userId": self.user1_id},
                                          headers = self.auth_header)
        manager_id = manager_response.json()["id"]
        self.manager_id = manager_id

    def create_band(self):
        band_response = self.client.post("http://localhost:9092/api/v1/bands", json={"name": "New Band",
                                                                                     "genre" : "Rock",
                                                                                     "bandManagerId": self.manager_id},
                                                                               headers = self.auth_header)
        band_id = band_response.json()["id"]
        self.band_id = band_id
    def send_band_proposal(self):
        proposal_response = self.client.post("http://localhost:9094/api/v1/proposals", json={"bandId": self.band_id,
                                                                                              "userId": self.user2_id,
                                                                                              "resolved": False},
                                                                                         headers = self.auth_header)
        proposal_id = proposal_response.json()["id"]
        self.proposal_id = proposal_id

    def accept_band_proposal(self):
        self.client.post(f"http://localhost:9091/api/v1/users/proposals/{self.proposal_id}/accept",headers = self.auth_header)


    @task
    def scenario(self):
        self.create_users()
        self.assign_band_manager()
        self.create_band()
        self.send_band_proposal()
        self.accept_band_proposal()


class MyUser(HttpUser):
    tasks = [UserService]
    wait_time = between(1, 3)
