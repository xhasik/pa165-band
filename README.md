# PA165 semestral project
 
## Name
Music Band Manager

## Description
The web application allows one music band to manage their activities. Managers can create a new band and start hiring team members from a catalogue. They can set for the band the musical style (e.g. rock, pop, etc…), a name, and a logo. Band managers can add new albums and songs to a band. Each Song has a name and a duration. Managers can also schedule tours for the bands, in terms of dates and cities visited.
Team members can login to the system and see the offers made by the band managers. They can accept or reject to be part of a band. After they are part of a band, they can see all the activities that are planned and the profiles of the other band members.

## Cloning the projects
To clone all the projects, you can simply run this command in shell (must be cmd for Windows) in the folder of your choice:

git clone https://gitlab.fi.muni.cz/xskipala/pa165-band-api-gw.git && \
git clone https://gitlab.fi.muni.cz/xskipala/pa165-band-management-service.git && \
git clone https://gitlab.fi.muni.cz/xskipala/pa165-band-manager-service.git && \
git clone https://gitlab.fi.muni.cz/xskipala/pa165-team-member-service.git && \
git clone https://gitlab.fi.muni.cz/xskipala/pa165-user-service.git

## Installing and running all the projects using docker with one command
Unfortunately, you cannot perform a lot of operations when the services are running in docker, since they can't communicate
to each other. If you want to perform the special operation, you will have to run the services locally.

Use this command in the folder with the projects to run all of them in docker (detached mode), tested on MacOS (should work for unix):

For Unix:

cd ./pa165-band-api-gw/ && mvn clean install && docker-compose up --detach --build && cd .. && cd ./pa165-user-service/ && mvn clean install && docker-compose up --detach --build && cd .. && cd ./pa165-band-manager-service/ && mvn clean install && docker-compose up --detach --build && cd .. && cd ./pa165-band-management-service/ && mvn clean install && docker-compose up --detach --build && cd .. && cd ./pa165-team-member-service/ && mvn clean install && docker-compose up --detach --build && cd .. && docker ps

For Windows (must use cmd, not powershell):

cd .\pa165-band-api-gw\ && mvn clean install && docker-compose up --detach --build && cd .. && cd .\pa165-user-service\ && mvn clean install && docker-compose up --detach --build && cd .. && cd .\pa165-band-manager-service\ && mvn clean install && docker-compose up --detach --build && cd .. && cd .\pa165-band-management-service\ && mvn clean install && docker-compose up --detach --build && cd .. && cd .\pa165-team-member-service\ && mvn clean install && docker-compose up --detach --build && cd .. && docker ps

## Swagger for all the services

THIS URL IS THE ONLY URL YOU NEED:

http://localhost:8084/swagger-ui/index.html - API GW that aggregates all the services (choose top right in the menu)

however, to access the individual ones, you can still go to these urls:

http://localhost:9091/swagger-ui/index.html - User Service

http://localhost:9092/swagger-ui/index.html - Band Management Service

http://localhost:9093/swagger-ui/index.html - Team Member Service

http://localhost:9094/swagger-ui/index.html - Band Manager Service

## Special operations

To try out the special operation that our project has, run band management service, team member service, user service and band manager service.
1. Create two users in user service (careful, you need to have proper email format)
2. Create a band manager in band manager service (userId 1)
3. Create a band in band management service (bandManagerId 1)
4. Create a proposal in band manager service (userId 2, bandId 1) CAREFUL, RESOLVED HAS TO BE SET AS FALSE
5. Get proposals from user service for userId 2
6. Accept the proposal with id from last response
7. Get band with id 1.. you will see that the band now has a team member!

The other special and nice to have thing is the API gateway. The API gateway runs on port 9090 and will parse the path of the request and send it over to the right service. No more struggles with remembering what ports are your 40 microservices running on. Simply fire all the requests to 9090 and the API gateway will handle it for you.

Project is separated to 4 microservices and an API gateway.

## Script

The special operation can now be run via locust.io script.
To run the script you need:
1. Have locust installed (you can use 'pip install locust')
2. Run all services via provided command using docker
3. Get your auth token in API Gateway
4. Navigate to the locust file
5. Change the token value in the locust file
6. Run locust via 'locust --headless --users 1 --host=http://localhost:9091 -f locustfile.py'

## API Gateway
https://gitlab.fi.muni.cz/xskipala/pa165-band-api-gw

## Management service
https://gitlab.fi.muni.cz/xskipala/pa165-band-management-service

## Band manager service
https://gitlab.fi.muni.cz/xskipala/pa165-band-manager-service

## Team-member service
https://gitlab.fi.muni.cz/xskipala/pa165-team-member-service

## User service
https://gitlab.fi.muni.cz/xskipala/pa165-user-service

## Class diagram
![image.png](./image.png)

## Use-case diagram
![image-1.png](./image-1.png)
